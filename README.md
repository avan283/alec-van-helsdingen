# README #


Public respository of Alec van Helsdingen

The ImageProcessing folder contains file for a Part I University project using MATLAB. 
Running the script in ConvertImages allows an image to be simplified to k colours. The k-means clustering algorithim is used.
Full marks were received for this project.  

The IrelandDail folder contains a Python file and 3 CSV files for calculations relating to the Irish Dail (Parliament), and forecasts of election results
under the STV voting system. 


The Microbit folder contains files to run on a BBC Microbit- a very small microprocesser- that plays the game Paper-Rock-Scissors against another microbit.
Parts of the code were provided by university instructors as part of the project. 

The writing folder contains three pieces of writing I have produced over the last two years. All were written individually, though two (the 263OR and 263CM reports) drew on work
done as a group. 

The Simplex Method folder contains three folders of MATLAB files. These files implement three different versions of the (Revised) Simplex Method for solving
Linear Programs. The first folder (1a), implements the simplex method given a known starting solution, 1b implements the simplex method without a starting
solution, and 1c implements the full RSM Algorithim by George Dantzig. 



